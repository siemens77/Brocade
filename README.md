PRTG Device Template for Brocade Ethernet Switches devices
===========================================

This project contains all the files necessary to integrate the Brocade Ethernet Switches
into PRTG for auto discovery and sensor creation.

This template is based on the Foundry MIB.

It will create sensors for monitoring status of:
 Chassis Health
		ch1[snAgentCpuUtilValue]: The statistical CPU utilization in units of one-hundredth&#x0d;
		 of a percent. This value is deprecated. Users are recommended&#x0d;
		 to use snAgentCpuUtilPercent or snAgentCpuUtil100thPercent&#x0d;
		 instead.&#x0d;
		ch2[snAgGblCpuUtil1MinAvg]: The statistics collection of 1 minute CPU utilization.&#x0d;
		ch3[snAgGblDynMemUtil]: The system dynamic memory utilization, in unit of percentage. Deprecated: Refer to snAgSystemDRAMUtil&#x0d;
		ch4[snAgGblDynMemFree]:	The free amount of system dynamic memory, in number of bytes. Deprecated: Refer to snAgSystemDRAMFree&#x0d;
		ch5[snAgGblResourceLowWarning]:	false - No, the device does not have resource-low-warning. true  - Yes, the device does have resource-low-warning.&#x0d;
		ch6[snAgGblExcessiveErrorWarning]: false - No, the device does not have any excessive collision, FCS errors, alignment warning etc. true  - Yes, the device does have.&#x0d;
 Power Supplies
 Fans
 Temperature
 

Download Instructions
=========================
 A zip file containing all the files in the project can be downloaded from the 
repository(https://gitlab.com/PRTG/Device-Templates/Brocade/-/jobs/artifacts/master/download?job=PRTGDistZip) 
download link

Installation Instructions
=========================
Please refer to INSTALL.md
